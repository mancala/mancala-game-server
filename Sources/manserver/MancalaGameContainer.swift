//
//  MancalaGameContainer.swift
//
//  Created by Michael Sanford on 1/21/17.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation
import GamingCloudGameContainer
import MancalaGameEngine
import GamingCore
import NetworkingCore
import MancalaCore
import MancalaCloudCore


// TODO: have the init take in a 'Data' which is the CreateGameRequest payload
//       may be able to get rid of some playerInfo methods
//       GamePlayerInfo *may* be able to turn into a protocol
public class MancalaGameContainer: RealTimeGameServerContainerProtocol {
    private let domain = "MancalaGameContainer"
    private let numberOfMovesForEarnings: Int = 2
    
    private let gameService: GameServiceRef
    private let game: Game
    
    private let player1: MancalaServerPlayerInfo
    private let player2: MancalaServerPlayerInfo
    
    private var player1JoinedGame: Bool = false
    private var player2JoinedGame: Bool = false
    private var isGameDetermined: Bool = false
    
    private let startupGameInfo: MancalaGameClientInfo
    
    public required init?(gameService: GameServiceRef, gameStartupData: Data) {
        self.gameService = gameService
        
        let unarchiver = ByteUnarchiver(archive: gameStartupData)
        guard let gameStartupInfo = GameStartupInfo(unarchiver: unarchiver) else { return nil }
        
        player1 = gameStartupInfo.player1Info
        player2 = gameStartupInfo.player2Info
        
        game = Game(startingTurnLogic: gameStartupInfo.startingLogic, gameOverOnWinner: true, player1: player1.type, player2: player2.type)
        game.container = gameService
        game.turnTimeLimit = 45
        game.countdownTime = 20
        guard let startingPlayer = game.currentGameState.currentPlayer else { return nil }
        startupGameInfo = MancalaGameClientInfo(startingPlayer: startingPlayer)

        game.botDidMakeMoveHandler = { [weak self] in
            self?.onBotDidMakeMove(withIdentity: $0, gameMove: $1)
        }
        game.playerDidTimeoutTurnHandler = { [weak self] (player, _) in
            self?.onTurnDidTimeout(withPlayer: player)
        }
        game.playerDidStartTurnCountdownHandler = { [weak self] in
            self?.onPlayerDidStartCountdown(withPlayer: $0, seconds: $1)
        }
    }
    
    public var playerTypes: [PlayerID: GamingCloudGameContainer.PlayerType] {
        return [player1, player2].enumerated().reduce([:]) { result, entry in
            var updatedResult = result
            updatedResult[entry.offset] = entry.element.type.isHuman ? .human : .bot
            return updatedResult
        }
    }
        
    public var gameInfo: Data {
        return ByteArchiver.archive(for: startupGameInfo)
    }
    
    var playerCount: Int {
        return 2
    }
    
    private func player(for identity: PlayerIdentity) -> MancalaServerPlayerInfo {
        switch identity {
        case .one: return player1
        case .two: return player2
        }
    }
    
    private func playerID(for identity: PlayerIdentity) -> PlayerID {
        switch identity {
        case .one: return 0
        case .two: return 1
        }
    }
    
    private func determineGameOutcomeIfNeeded() {
        
        struct DeltaTierInfo {
            let win: Int
            let tie: Int
            let loss: Int
        }
        
        func gamesPlayedTier(_ numberOfGamesPlayed: Int) -> Int {
            switch min(0, numberOfGamesPlayed) {
            case 0 ..< 10:
                return 2
            case 10 ..< 99:
                return 1
            default:
                return 0
            }
        }
        
        guard !isGameDetermined else { return }
        
        let winner: PlayerIdentity?
        let startingMoves: StartingMoves
        let loserSkippedTurn: Bool
        switch game.currentGameState {
        case .readyToPlay, .cancelled:
            return
        case .inProgress(_, let gameWinner, let gameStartingMoves):
            guard let gameWinner = gameWinner else { return }
            winner = gameWinner
            startingMoves = gameStartingMoves
            loserSkippedTurn = false
        case .gameOver(let gameWinner, let reason, let gameStartingMoves):
            winner = gameWinner
            startingMoves = gameStartingMoves
            loserSkippedTurn = reason.isSkippedTurn
        }
        
        isGameDetermined = true
        
        let p1DeltaTierInfo: DeltaTierInfo
        let p2DeltaTierInfo: DeltaTierInfo
        let delta = player1.info.points - player2.info.points
        let p1GamesPlayedTier = gamesPlayedTier(player1.info.numberOfGamesPlayed)
        let p2GamesPlayedTier = gamesPlayedTier(player2.info.numberOfGamesPlayed)
        
        let isP1Stronger = delta > 0
        switch abs(delta) {
        case 0 ..< 60:
            p1DeltaTierInfo = DeltaTierInfo(
                win: p1GamesPlayedTier + 1,
                tie: p1GamesPlayedTier,
                loss: p1GamesPlayedTier)
            p2DeltaTierInfo = DeltaTierInfo(
                win: p2GamesPlayedTier + 1,
                tie: p2GamesPlayedTier,
                loss: p2GamesPlayedTier)
        case 60 ..< 180:
            p1DeltaTierInfo = DeltaTierInfo(
                win: isP1Stronger ? p1GamesPlayedTier + 1 : p1GamesPlayedTier + 2,
                tie: isP1Stronger ? p1GamesPlayedTier : p1GamesPlayedTier + 1,
                loss: isP1Stronger ? p1GamesPlayedTier : p1GamesPlayedTier + 1)
            p2DeltaTierInfo = DeltaTierInfo(
                win: !isP1Stronger ? p1GamesPlayedTier + 1 : p1GamesPlayedTier + 2,
                tie: !isP1Stronger ? p1GamesPlayedTier : p1GamesPlayedTier + 1,
                loss: !isP1Stronger ? p1GamesPlayedTier : p1GamesPlayedTier + 1)
        default:
            p1DeltaTierInfo = DeltaTierInfo(
                win: isP1Stronger ? p1GamesPlayedTier : p1GamesPlayedTier + 3,
                tie: isP1Stronger ? p1GamesPlayedTier - 1 : p1GamesPlayedTier + 2,
                loss: isP1Stronger ? p1GamesPlayedTier - 1 : p1GamesPlayedTier + 1)
            p2DeltaTierInfo = DeltaTierInfo(
                win: !isP1Stronger ? p1GamesPlayedTier : p1GamesPlayedTier + 3,
                tie: !isP1Stronger ? p1GamesPlayedTier - 1 : p1GamesPlayedTier + 2,
                loss: !isP1Stronger ? p1GamesPlayedTier - 1 : p1GamesPlayedTier + 1)
        }
        
//        playtype_id | coins
//       -------------+-------
//        win.hard    |    15
//        win.normal  |    10
//        win.easy    |     6
//        lose.hard   |     6
//        lose.normal |     5
//        lose.easy   |     2
//        tie         |     6
        
        // detemined winner or tie game
        let p1PointsEarned: Int
        let p1CoinsEarned: Int
        let p2PointsEarned: Int
        let p2CoinsEarned: Int
        let p1PointsRandom: Int
        let p2PointsRandom: Int
        let p1DeltaTier: Int
        let p2DeltaTier: Int
        var p1SkippedTurn: Bool = false
        var p2SkippedTurn: Bool = false
        
        if let winner = winner {
            let winnerCoins: Int
            let loserCoins: Int
            switch (game.startingPlayer == winner, startingMoves.is41) {
            case (_, false):
                // normal game
                winnerCoins = 10
                loserCoins = loserSkippedTurn ? 0 : 5
            case (true, true):
                // cheater won game
                winnerCoins = 6
                loserCoins = loserSkippedTurn ? 0 : 6
            case (false, true):
                // cheater lost game
                winnerCoins = 14
                loserCoins = loserSkippedTurn ? 0 : 2
            }
            if winner == .one {
                p1CoinsEarned = winnerCoins
                p2CoinsEarned = loserCoins
                p1PointsRandom = startingMoves.is41 ? 0 : Int(UInt8.random(2)) + 1
                p2PointsRandom = 0
                p1DeltaTier = p1DeltaTierInfo.win
                p2DeltaTier = p2DeltaTierInfo.loss
                p2SkippedTurn = loserSkippedTurn
            } else {
                p1CoinsEarned = loserCoins
                p2CoinsEarned = winnerCoins
                p1PointsRandom = 0
                p2PointsRandom = startingMoves.is41 ? 0 : Int(UInt8.random(2)) + 1
                p1DeltaTier = p1DeltaTierInfo.loss
                p2DeltaTier = p2DeltaTierInfo.win
                p1SkippedTurn = loserSkippedTurn
            }
        } else {
            // tie game
            p1CoinsEarned = 6
            p2CoinsEarned = 6
            p1PointsRandom = Int(UInt8.random(2)) + 1
            p2PointsRandom = Int(UInt8.random(2)) + 1
            p1DeltaTier = p1DeltaTierInfo.tie
            p2DeltaTier = p2DeltaTierInfo.tie
        }
        
        let pointMax = Int(UInt8.random(3)) + 10
        let pointMin = 1
        
//        p1PointsEarned = min(pointMax, max(pointMin, ((p1CoinsEarned / 3) * p1DeltaTier))) + p1PointsRandom
//        p2PointsEarned = min(pointMax, max(pointMin, ((p2CoinsEarned / 3) * p2DeltaTier))) + p2PointsRandom
        p1PointsEarned = p1SkippedTurn ? 0 : min(pointMax, max(pointMin, ((p1CoinsEarned / 3)))) + p1PointsRandom
        p2PointsEarned = p2SkippedTurn ? 0 : min(pointMax, max(pointMin, ((p2CoinsEarned / 3)))) + p2PointsRandom
        log(.debug, domain, "delta tiers=(\(p1DeltaTier), \(p2DeltaTier)")
        
        if player(for: .one).type.isHuman && !p1SkippedTurn {
            let p1Payload = PlayerDidEarnPointsAndCoins(pointsEarned: p1PointsEarned, coinsEarned: p1CoinsEarned)
            let p1Message = Message(type: PlayerDidEarnPointsAndCoins.type, payload: p1Payload)
            gameService.send(message: p1Message, toPlayer: playerID(for: .one))
        }
        
        if player(for: .two).type.isHuman && !p2SkippedTurn {
            let p2Payload = PlayerDidEarnPointsAndCoins(pointsEarned: p2PointsEarned, coinsEarned: p2CoinsEarned)
            let p2Message = Message(type: PlayerDidEarnPointsAndCoins.type, payload: p2Payload)
            gameService.send(message: p2Message, toPlayer: playerID(for: .two))
        }
    }
    
    private func onDidMakeMove(with pitID: PitID, byPlayer playerIdentity: PlayerIdentity) {
        let payloadMessage = PlayerDidMakeMoveMessage(playerIdentity: playerIdentity, pitID: pitID)
        let message = Message(type: PlayerDidMakeMoveMessage.type, payload: payloadMessage)
        
        let opponentPlayerIdentity = playerIdentity.opponentIdentity
        let opponentPlayer = player(for: opponentPlayerIdentity)
        if opponentPlayer.type.isHuman {
            log(.debug, domain, "sending PlayerDidMakeMoveMessage to \(opponentPlayerIdentity)")
            gameService.send(message: message, toPlayer: playerID(for: opponentPlayerIdentity))
        }
        gameService.sendSpectators(message: message)
    }
    
    private func onDidSendChatMessage(_ didSendChatPayload: PlayerDidSendChatPayload) {
        let message = Message(type: PlayerDidSendChatPayload.serverToClientType, payload: didSendChatPayload)
        let opponentPlayerIdentity = didSendChatPayload.fromPlayer.opponentIdentity
        let opponentPlayer = self.player(for: opponentPlayerIdentity)
        if opponentPlayer.type.isHuman {
            log(.debug, domain, "sending PlayerDidSendChatPayload to \(opponentPlayerIdentity)")
            gameService.send(message: message, toPlayer: playerID(for: opponentPlayerIdentity))
        }
        gameService.sendSpectators(message: message)
    }
    
    private func onDidInteractWithProp(_ didInteractWithPropMessage: PlayerDidInteractWithPropMessage) {
        let message = Message(type: PlayerDidInteractWithPropMessage.serverToClientType, payload: didInteractWithPropMessage)
        
        let opponentPlayerIdentity = didInteractWithPropMessage.playerIdentity.opponentIdentity
        let opponentPlayer = self.player(for: opponentPlayerIdentity)
        if opponentPlayer.type.isHuman {
            log(.debug, domain, "sending PlayerDidInteractWithPropMessage to \(opponentPlayerIdentity)")
            gameService.send(message: message, toPlayer: playerID(for: opponentPlayerIdentity))
        }
        gameService.sendSpectators(message: message)
    }
    
    private func onDidSkipTurn(_ didSkipTurnPayload: PlayerDidSkipTurn) {
        let message = Message(type: PlayerDidSkipTurn.serverToClientType, payload: didSkipTurnPayload)
        
        let opponentPlayerIdentity = didSkipTurnPayload.playerIdentity.opponentIdentity
        let opponentPlayer = self.player(for: opponentPlayerIdentity)
        if opponentPlayer.type.isHuman {
            log(.debug, domain, "sending PlayerDidSkipTurn to \(opponentPlayerIdentity)")
            gameService.send(message: message, toPlayer: playerID(for: opponentPlayerIdentity))
        }
        gameService.sendSpectators(message: message)
        
//        game.skipTurn()
//        game.makeBotMoveIfNeeded()
        game.forceGameOverOnSkippedTurn(withWinner: opponentPlayerIdentity)
        onDidCompleteMove()
    }
    
    func onBotDidMakeMove(withIdentity botIdentity: PlayerIdentity, gameMove: GameMove) {
        guard let pitID = gameMove.transitions.first?.seedTransitions.first?.fromPit.identifier else {
            assert(false, "unable to determine move pit")
            return
        }
        log(.debug, domain, "bot did make move")
        onDidMakeMove(with: pitID, byPlayer: botIdentity)
        onDidCompleteMove()
    }
    
    func onDidCompleteMove() {
//        print("\(game.debugDescription)")
        
        determineGameOutcomeIfNeeded()
        game.makeBotMoveIfNeeded()
        
        if !game.currentGameState.isPlayable {
            player1JoinedGame = !player1.type.isHuman
            player2JoinedGame = !player2.type.isHuman
            
            // TODO: fix bug here where both players are bots. Needs to notify the parent that the players have joined
        }
    }
    
    func onPlayerDidStartCountdown(withPlayer player: PlayerIdentity, seconds: Int) {
        let countdownPayload = PlayerDidStartTurnCountdown(playerIdentity: player, seconds: seconds - 4)
        let opponentCountdownPayload = PlayerDidStartTurnCountdown(playerIdentity: player, seconds: seconds - 3)
        let slowPlayerMessage = Message(type: PlayerDidStartTurnCountdown.type, payload: countdownPayload)
        let opponentMessage = Message(type: PlayerDidStartTurnCountdown.type, payload: opponentCountdownPayload)
        let slowPlayerID = self.playerID(for: player)
        let opponentPlayerID = self.playerID(for: player.opponentIdentity)
        gameService.send(message: slowPlayerMessage, toPlayer: slowPlayerID)
        gameService.send(message: opponentMessage, toPlayer: opponentPlayerID)
        gameService.sendSpectators(message: opponentMessage)
    }
    
    func onReceived(message: Message, from playerID: PlayerID) {
        guard let type = MancalaClientToGameServerMessageType(rawValue: message.type) else {
            assert(false, "received message with invalid type (\(message.type))")
            return
        }
        switch type {
        case .playerDidInteractWithProp:
            guard let payload = message.payload else {
                assert(false, "received PlayerDidInteractWithPropMessage message with empty payload")
                return
            }
            let unarchiver = ByteUnarchiver(archive: payload)
            guard let didInteractWithPropMessage = PlayerDidInteractWithPropMessage(unarchiver: unarchiver) else {
                assert(false, "failed to unarchive PlayerDidInteractWithPropMessage message")
                return
            }
            log(.debug, domain, ">>> received: playerDidInteractWithProp (\(didInteractWithPropMessage.playerIdentity)")
            onDidInteractWithProp(didInteractWithPropMessage)
            
        case .playerDidSendChatMessage:
            guard let payload = message.payload else {
                assert(false, "received PlayerDidInteractWithPropMessage message with empty payload")
                return
            }
            let unarchiver = ByteUnarchiver(archive: payload)
            guard let didSendChatPayload = PlayerDidSendChatPayload(unarchiver: unarchiver) else {
                assert(false, "failed to unarchive PlayerDidSendChatPayload payload")
                return
            }
            log(.debug, domain, ">>> received: PlayerDidSendChatPayload (from=\(didSendChatPayload.fromPlayer) | message=\(didSendChatPayload.message)")
            onDidSendChatMessage(didSendChatPayload)
            
        case .playerDidSkipTurn:
            guard let payload = message.payload else {
                assert(false, "received PlayerDidSkipTurn message with empty payload")
                return
            }
            let unarchiver = ByteUnarchiver(archive: payload)
            guard let didSkipTurnPayload = PlayerDidSkipTurn(unarchiver: unarchiver) else {
                assert(false, "failed to unarchive PlayerDidSkipTurn payload")
                return
            }
            log(.debug, domain, ">>> received: didSkipTurnPayload (\(didSkipTurnPayload.playerIdentity)")
            onDidSkipTurn(didSkipTurnPayload)

        case .playerDidRequestMove:
            guard let payload = message.payload else {
                assert(false, "received playerDidRequestMove message with empty payload")
                return
            }
            let unarchiver = ByteUnarchiver(archive: payload)
            guard let moveRequestMessage = PlayerDidRequestMoveMessage(unarchiver: unarchiver) else {
                assert(false, "failed to unarchive playerDidRequestMove message")
                return
            }
            
            guard game.currentGameState.currentPlayer == moveRequestMessage.playerIdentity else {
//                assert(false, "move request from wrong player (not their turn)")
                return
            }
            log(.debug, domain, ">>> received: playerDidRequestMove (\(moveRequestMessage.playerIdentity)")
            do {
                try game.makeMove(fromPitID: moveRequestMessage.pitID)
                onDidMakeMove(with: moveRequestMessage.pitID, byPlayer: moveRequestMessage.playerIdentity)
                onDidCompleteMove()
            } catch (let error) {
                log(.warning, domain, "failed to make move \(error)")
            }
            
        case .playerDidRequestToPlayAgain:
            guard let payload = message.payload else {
                assert(false, "received playerDidRequestToPlayAgain message with empty payload")
                return
            }
            let unarchiver = ByteUnarchiver(archive: payload)
            guard let playAgainRequestMessage = PlayerDidRequestToPlayAgainMessage(unarchiver: unarchiver) else {
                assert(false, "failed to unarchive playAgainRequestMessage message")
                return
            }
//            guard !game.currentGameState.isPlayable else {
//                assert(false, "playerDidRequestToPlayAgain when game is not over")
//                return
//            }
            
            if game.currentGameState.isPlayable {
                log(.warning, domain, "playerDidRequestToPlayAgain when game is not over")
            }
            log(.debug, domain, ">>> received: playerDidRequestToPlayAgain (\(playAgainRequestMessage.playerIdentity)")
            switch playAgainRequestMessage.playerIdentity {
            case .one:
                player1JoinedGame = true
            case .two:
                player2JoinedGame = true
            }
            if player1JoinedGame && player2JoinedGame {
                onAllHumanPlayersJoined()
            }
        }
    }
    
    public func onGameStateUpdate(withTime time: Timestamp32, messages: [PlayerMessage]) {
        guard !messages.isEmpty else { return }
        messages.forEach { [weak self] in self?.onReceived(message: $0.message, from: $0.playerID) }
    }
    
    public func onGameStateUpdate(withTime time: Timestamp32, playerStreams: [PlayerID: StreamData]) -> BroadcastStreamData {
        return .all(data: nil)
    }
    
    public func onAllHumanPlayersJoined() {
        func sendPlayerGameDidStartMessage(forPlayerID playerID: PlayerID, startingPlayer: PlayerIdentity, gameboardID: String, startTime: Timestamp64) {
            guard let gameToken = gameService.gameToken(forPlayerID: playerID) else {
                //assert(false, "player \(playerID) joined state is horked at start of game")
                log(.warning, domain, "player \(playerID) joined state is horked at start of game")
                gameService.endGameSession()
                return
            }
            let didStartMessage = GameDidStartMessage(gameToken: gameToken, startingPlayer: startingPlayer, gameboardID: gameboardID, startTime: startTime)
            let message = Message(type: GameDidStartMessage.type, payload: didStartMessage)
            log(.debug, domain, "sending GameDidStartMessage to player (playerID=\(playerID))")
            gameService.send(message: message, toPlayer: playerID)
        }
        
        player1JoinedGame = true
        player2JoinedGame = true
        isGameDetermined = false
        
        game.reset()
        let botDelay: TimeInterval = TimeInterval(UInt8.random(4) + 2)
        game.playGame(botDelay: botDelay)
        
        guard case .readyToPlay(let startingPlayer) = game.currentGameState else {
            assert(false, "game state is horked at start of game")
            return
        }
        
        // set gameboard to the gameboard of the starting player
        let gameboardID: String
        switch startingPlayer {
        case .one:
            gameboardID = player1.info.gameboardID
        case .two:
            gameboardID = player2.info.gameboardID
        }
        
        let now: Timestamp64 = Date.now
        if player1.type.isHuman {
            sendPlayerGameDidStartMessage(forPlayerID: 0, startingPlayer: startingPlayer, gameboardID: gameboardID, startTime: now)
        }
        if player2.type.isHuman {
            sendPlayerGameDidStartMessage(forPlayerID: 1, startingPlayer: startingPlayer, gameboardID: gameboardID, startTime: now)
        }
        
        let gameToken = gameService.gameTokenForSpectator
        let didStartMessage = GameDidStartMessage(gameToken: gameToken, startingPlayer: startingPlayer, gameboardID: gameboardID, startTime: now)
        let message = Message(type: GameDidStartMessage.type, payload: didStartMessage)
        gameService.sendSpectators(message: message)
    }
    
    public func onPlayerDidJoin(withPlayerID playerID: PlayerID, playerType: ConnectedPlayerType, playerInfoPayload: Data?) {
        switch playerID {
        case 0 where player1JoinedGame == false:
            player1JoinedGame = true
        case 1 where player2JoinedGame == false:
            player2JoinedGame = true
        default:
            assert(false, "invalid state in onPlayerDidJoin")
        }
//        if player1JoinedGame && player2JoinedGame {
//            onAllHumanPlayersJoined()
//        }
    }
    
    func onTurnDidTimeout(withPlayer playerIdentity: PlayerIdentity) {
        log(.debug, domain, "ending game session because player turn timed out.")

        if game.numberOfMoves >= numberOfMovesForEarnings {
            game.forceGameOverOnSkippedTurn(withWinner: playerIdentity.opponentIdentity)
            determineGameOutcomeIfNeeded()
        } else {
            game.cancelGame()
        }
        
        let iLeftMessage = Message(type: PlayerDidLeaveGameMessage.type, payload: PlayerDidLeaveGameMessage(playerIdentity: playerIdentity))
        let opponentLeftMessage = Message(type: PlayerDidLeaveGameMessage.type, payload: PlayerDidLeaveGameMessage(playerIdentity: playerIdentity.opponentIdentity))
        log(.debug, domain, "unexpected onTurnDidTimeout. broadcasting PlayerDidLeaveGameMessage to all players and spectators. ")
        
        let myID = playerID(for: playerIdentity)
        let opponentID = playerID(for: playerIdentity.opponentIdentity)
        
        let playerMessages: [PlayerID: [Message]] = [
            myID: [opponentLeftMessage],
            opponentID: [iLeftMessage]
        ]
        
        let allMessages = BroadcastMessages.playerSpecialized(playerMessages: playerMessages, spectatorMessages: [iLeftMessage, opponentLeftMessage])
        
        gameService.broadcast(messages: allMessages) { [weak self] in
            log(.debug, self?.domain ?? "nil", "ending game session")
            self?.gameService.endGameSession()
        }
    }
    
    public func onPlayerDidLeave(withID playerID: PlayerID, remainingHumanPlayerCount: Int, spectatorCount: Int) {
        //        guard remainingHumanPlayerCount + spectatorCount > 0 else {
        //            game.cancelGame()
        //            gameService.endGameSession()
        //            return
        //        }
//        guard let playerIdentity = self.playerIdentity(forPlayerID: playerID), let opponentPlayerID = self.opponentPlayerID(for: playerID) else { return }
        guard let playerIdentity = self.playerIdentity(forPlayerID: playerID) else { return }
        if game.numberOfMoves >= numberOfMovesForEarnings {
            game.forceGameOverOnSkippedTurn(withWinner: playerIdentity.opponentIdentity)
            determineGameOutcomeIfNeeded()
        } else {
            game.cancelGame()
        }
        let message = Message(type: PlayerDidLeaveGameMessage.type, payload: PlayerDidLeaveGameMessage(playerIdentity: playerIdentity))
        log(.debug, domain, "broadcasting PlayerDidLeaveGameMessage (leavingPlayer=\(playerIdentity)")
        gameService.broadcast(messages: BroadcastMessages.all(messages: [message])) { [weak self] in
            log(.debug, self?.domain ?? "nil", "ending game session")
            self?.gameService.endGameSession()
        }
        
//        gameService.send(message: message, toPlayer: opponentPlayerID)
//        gameService.sendSpectators(message: message)
//        gameService.endGameSession()
    }
    
    func opponentPlayerID(for playerID: PlayerID) -> PlayerID? {
        switch playerID {
        case 0:
            return 1
        case 1:
            return 0
        default:
            return nil
        }
    }
    
    func playerIdentity(forPlayerID playerID: PlayerID) -> PlayerIdentity? {
        switch playerID {
        case 0:
            return .one
        case 1:
            return .two
        default:
            log(.warning, domain, "found invalid playerID (playerID=\(playerID))")
            return nil
        }
    }
}
