import GameServer
import GamingCloudGameContainer
import NetworkingCore

logLevel = .debug
runGameServer(
    with: GameServerConfig.ServerType.realtime(MancalaGameContainer.self as RealTimeGameServerContainerProtocol.Type),
    gameTimeLimit: Timestamp32(minutes: 31)
)
