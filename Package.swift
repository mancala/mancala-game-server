// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "manserver",
    dependencies: [
        .package(url: "git@gitlab.com:mancala/gaming-cloud-server.git", from: "1.1.0"),
        .package(url: "git@gitlab.com:mancala/mancala-cloud-core.git", from: "1.1.0"),
        .package(url: "git@gitlab.com:mancala/mancala-game-engine.git", from: "1.2.0"),
    ],
    targets: [
        .target(
            name: "manserver",
            dependencies: ["GameServer", "MancalaCloudCore", "MancalaGameEngine"]),
    ]
)
